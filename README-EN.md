# Git exercise

The following practical exercises should familiarize you with the use of Git.

Note: the command-line examples provided are there to remind you of the correct syntax and keywords, and won't necessarily work if you type them in blindly.

Another note: for help with a particular git command, use `git <command> --help` or `man git-<command>`.

## Installation

1. Install and configure git on your computer [git site page](https://git-scm.com/download): 
   
   For Linux :
   ```bash
   sudo apt/dnf/... install git
   ```

   For Mac 🤢 :
   ```bash
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   brew install git
   ```

   For Windows 🤮 :
   ```bash
   winget install --id Git.Git -e --source winget 
   ```

## Configuration

### Configure its identity

   ```bash
   git config --global user.name "<Firstname Lastname>"
   git config --global user.email "<id@bde.enseeiht.fr>"
   ```

### Adding an SSH key

   Although net7's git allows it, the functionality to connect via http is disabled on all other sites. The connection method involves sharing an SSH key. You therefore need to generate one and add it to your git account.

#### Generation
   To generate keys (public and private) :
   ```bash
   ssh-keygen -t ed25519 -C "identifiant@bde.enseeiht.fr"
   ```
   Once saved under the default name (id_ed25519) or the name you have entered, the public key must be displayed:
   ```bash
   cat ~/.ssh/<key_name>.pub
   ```
   Then copy and paste the entire result.

#### Adding the public key to your account

   Connect to INP-net's GitLab at <https://git.inpt.fr/users/sign_in>.
   
   Add the key (small button at top right) to <https://git.inpt.fr/-/profile/keys>. Copy and paste the cat result into the "Key" field and you're done.

   Start ssh-agent and add the key: 
   For Linux:
   ```bash
   eval "$(ssh-agent -s)"
   ssh-add ~/.ssh/<key_name>
   ```
   For Windows: https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement
   ```powerhell
   # By default the ssh-agent service is disabled. Configure it to start automatically.
   # Make sure you're running as an Administrator.
   Get-Service ssh-agent | Set-Service -StartupType Automatic

   # Start the service
   Start-Service ssh-agent

   # This should return a status of Running
   Get-Service ssh-agent

   # Now load your key files into ssh-agent
   ssh-add $env:USERPROFILE\.ssh\id_ed25519
   ```

## Exercise 0 : Preliminary step

### ADD FILES AND/OR FOLDERS IN TEST

**FIRST, CLICK ON THE LITTLE LINK AT THE TOP JUST BELOW THE REPOSITORY TO JOIN THE PROJECT**.
![image](img/Screenshot%20from%202023-10-05%2017-57-16.png)

*I've already joined the project, so it's marked "Leave", but it's in the same place for you*.

Clone the repository on your computer so you can work on it
```bash
git clone git@git.inpt.fr:net7/git-exercises.git
```

**Don't forget to move to the `cd git-exercises` directory**.

For the sake of this poor directory, you're going to create a branch in your name:
```bash
git checkout -b <new_branch>
```
example: `git checkout -b ma-jolie-branche` (values between chevrons can be replaced by whatever you like, chevrons included)  
This will enable you to take your first steps in complete safety 👍

## Exercise 1: First steps

1. Create a new file using your favorite editor: nano, ed, vim
   *(maybe vim training one day o^o)*
   
   ```bash
   nano <file>
   ```

2. After any significant changes, add the new file(s)

   ```bash
   git add <file or folder>
   ```

3. Validate the new files

   ```bash
   git commit -m "<commit-significant-message>"
   ```

4. (optional) Examine the state of your repo with `git status` and/or git fetch before and after adding a modification to the previous file (redo the git add and tralala)

   ```bash
   git status
   git fetch
   ```

5. (optional) Perform other commits and consult the log

   ```bash
   git log
   ```

6. Commit everything you've done so far

   ```bash
   git add .
   git commit -m "<commit-significant-message>"
   ```
   **!!! `git add .` it's tough, use with care when you're not alone on the repo!!!**

7. (optional) Validate new changes, but undo them by returning to the last commit:
   ```bash
   git reset HEAD~
   ```
   if you only have `add` and not `commit`, a simple `git checkout -- .` will suffice.

8.  Push the commits onto the server
      
      the first time:
      ```bash
      git push --set-upstream origin <new_branch>
      ```
      thereafter:

      ```bash
      git push
      ```

## Exercise 2: Branching and merging

1. Create a new branch from your first branch (yes, we're starting to freestyle)

   ```bash
   git checkout -b <new_branch>
   ```

2. Edit your new file and validate the result (as in exercise 1, this is to see if you're following or just typing commands in no brain mode 👀)

3. Return to the first branch

   ```bash
   git checkout <first_branch_name>
   ```

4. Merge `new_branch` into `first_branch`.

   ```bash
   git merge <new_branch>
   ```

5. You can now delete the merged branch:
   ```bash
   git branch -d <new_branch>
   ```

6. Now create conflicting commits in `other_new_branch` and `first_branch` and try to merge them. Note that the conflict resolution markers will look like this.

   ```bash
   <<<<<<< HEAD
   Here is the new line in premiere_branche
   =======
   This is the new line in the
   >>>>>>> branch
   ```
   Hint: to create a conflict, in the same file, delete lines in <new_branch> and add a line instead in <first_branch>.

7. Resolve the conflict (i.e., edit the conflict markers to match what you want the file to be) and commit the result. Use `git log` to see the resulting commits on the main branch.

## Exercise 3: Collaboration

Now that you've had some fun on your branch, it's time to ask the git repo admins to accept your "magnificent" additions. 

To do this, simply follow the online procedure on your browser by clicking on the "Create Pull Request" button 🎉🎉🎉

## Thanks

- <https://github.com/martinjrobins/exercise>
- <https://git.inpt.fr/Zil0/formation-git>
- <https://wiki.python.org/moin/SimplePrograms>
- <https://github.com/pypa/sampleproject>
- <https://git.inpt.fr/fainsil/git-exercises>
- DeepL pour la trad en français psk @fainsil l'avait écrit en anglais et @lebihae avait la flemme de traduire à la main
   - I thought it was really well written too 🙃
      - in fact I used it too to retranslate this f*** doc in English xD