---
marp: true
paginate: true
math: katex
theme: default
---

<!-- theme_diapo : https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js -->
<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

<style global>
section::after {
  content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
}
</style>

<style>
img[alt~="center"] {
    display: block;
    margin: 0 auto;
}
</style>

<header>

# net7 course
</header>


# Gîte training
![image](Pougne_gite_eg.jpg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

## What is a Gîte?

- Tourist accommodation for holidaymakers traditionally located in the countryside
- Boosted by the post-war rural exodus
- Today, the rural tourism sector is growing
- Various eco-labels

---

<header>

# Introduction
</header>

## How to put it on Airbnb

1. Create an ad with **photo** and **price**.
2. Fill in the various features of the accommodation (type, bedrooms, bathroom, etc)
3. Write an *effective* and *attractive* description.
4. Take advantage of the money

---

<header>

# net7 course
</header>


![image](git.svg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

## What is Git?

- A decentralized version manager
- Created in 2005 by Linus Torvalds, the creator of Linux 
- Free and [open source](https://github.com/git/git) (GPLv2)
- Fast, reliable and scalable
- [Essential](https://trends.google.com/trends/explore?q=git,svn,mercurial), especially in the professional world
- A graph!

---


<header>

# Introduction
</header>

## Git, what's it really good for?


In a nutshell: to version a project
In a nutshell, git lets you :


- track each file independently
- keep track of project history
  - so you can go back at any time
- share work online via various servers (Gitlab, Github, etc.)
- propose modifications or additions to an open-source project
- and, above all, save your projects over the course of the year
  -

---

<header>

# Introduction
</header>

## [Communication protocols](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)

Git can use four distinct protocols to transfer data:

- Local: `file://`
- HTTP: `http(s)://` (port 80)
- Secure Shell: `ssh://` (port 22)
- Git: `git://` (port 9418)

---
<header>

# Git basics
</header>

## Basic git actions

Git works with 4 basic operations (and lots of more complex commands if you become a pro):

- update the directory on your machine: `pull`
- add modifications made to files: `add`
- validate previously added modifications: `commit`
- push validated changes to the server: `push`

---
<header>

# Git basics
</header>

<center>

# [The four phases of git](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_recording_changes_to_the_repository)
</center>

<div class="mermaid">
sequenceDiagram
  participant Untracked
  participant Unmodified
  participant Modified
  participant Staged
  Untracked->>Staged: Add the file
  Unmodified->>Modified: Edit the file
  Modified->>Staged: Stage the file
  Unmodified->>Untracked: Remove the file
  Staged->>Unmodified: Commit
</div>

---

<header>

# Git basics
</header>

<center>

## A Graph
</center>


<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch toto
  checkout toto
  commit
  commit
  checkout main
  commit
  checkout toto
  commit
  checkout main
  merge toto
  commit
  branch tutu
  checkout tutu
  commit
  branch titi
  checkout titi
  commit
  checkout main
  commit
  checkout titi
  commit
  checkout tutu
  merge titi
  commit
  checkout main
  merge tutu
  commit
</div>

---

<header>

# Exercices
</header>

## QR Code for Git repo exercises

![image](qr_code_forma_git_2.png)

---

<header>

# Appendix
</header>

## SSH keys

An SSH key is used to authenticate a machine (basically your PC). You need to generate a pair :

- a public key: the one you share with everyone else
- a private key: the one you keep safe on your machine

---

<header>

# Appendix
</header>


![image](./Public_key_encryption.svg "title-2") ![image](./Private_key_signing.svg)

---

<header>

# Appendix
</header>

## Precision for exercises

In the commands, a value between brackets must be replaced by a value of your choice **brackets included !!!**.

---

<header>

# Going further
</header>

## Frontends

- [GitHub](https://github.com/)
- [GitLab](https://gitlab.com)
- [Gitea](https://gitea.io/)
- [Gogs](https://gogs.io/)
- [Codeberg](https://codeberg.org/)
- [SourceHut](https://sr.ht/)

---
<header>

# Going further
</header>

## A few tools

- [pre-commit](https://pre-commit.com/)
- [GitKraken](https://www.gitkraken.com/)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [CI/CD](https://resources.github.com/ci-cd/)
- [Signature](https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits)
