---
marp: true
paginate: true
math: katex
theme: default
---

<!-- theme_diapo : https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js -->
<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

<style global>
section::after {
  content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
}
</style>

<style>
img[alt~="center"] {
    display: block;
    margin: 0 auto;
}
</style>

<header>

# Formation net7
</header>


# Formation gîte
![image](Pougne_gite_eg.jpg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

##  Un gîte, c'est quoi ?

- Un hébergement touristique pour vacanciers traditionnellement installé à la campagne
- Essort avec l'exode rural d'après guerre
- Actuellement le secteur du tourisme rural grandit
- Différents labels écologiques

---

<header>

# Introduction
</header>

##  Comment le mettre sur Airbnb

1. Créer une annonce avec **photo** et **prix**
2. Renseigner les différentes caractéristiques du logement (type, chambres, salle de bain, etc)
3. Rédiger une description *efficace* et *attrayante*
4. Profiter de la moula

---

<header>

# Formation net7
</header>


![image](git.svg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

##  Git, c'est quoi ?

- Un gestionnaire de versions décentralisé
- Créé en 2005 par Linus Torvalds, le créateur de Linux 
- Gratuit et [open source](https://github.com/git/git) (GPLv2)
- Rapide, fiable et scalable
- [Incontournable](https://trends.google.com/trends/explore?q=git,svn,mercurial), surtout dans le monde professionnel
- Un graphe !

---


<header>

# Introduction
</header>

##  Git, à quoi qu'est-ce que ça peut bien servir en fait ?

En bref : à versionner un projet
En moins bref, git permet :

- de suivre chaque fichier indépendamment
- de garder en mémoire l'historique du projet
  - et donc pouvoir revenir en arrière à tout moment
- partager son travail en ligne via divers serveurs (Gitlab, Github, ...)
- proposer des modifications ou des ajouts sur un projet open-source
- et surtout à sauvegarder ses TP/projets au cours de l'année
  - 

---

<header>

# Introduction
</header>

## [Protocoles de communication](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)

Git peut utiliser quatre protocoles distincts pour transférer des données:

- Local: `file://`
- HTTP: `http(s)://` (port 80)
- Secure Shell: `ssh://` (port 22)
- Git: `git://` (port 9418)

---
<header>

# Les bases de Git
</header>

## Les actions de bases de git

Git fonctionne avec 4 opérations de base (et plein de commandes plus complexes si vous devenez des pros):

- mettre à jour le répertoire de votre machine : `pull`
- ajouter les modifications effectuées sur les fichiers : `add`
- valider les modifications précédemment ajoutées : `commit`
- pousser les modifications validées sur le serveur : `push`

---
<header>

# Les bases de Git
</header>

<center>

# [Les quatre phases de git](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_recording_changes_to_the_repository)
</center>

<div class="mermaid">
sequenceDiagram
  participant Untracked
  participant Unmodified
  participant Modified
  participant Staged
  Untracked->>Staged: Add the file
  Unmodified->>Modified: Edit the file
  Modified->>Staged: Stage the file
  Unmodified->>Untracked: Remove the file
  Staged->>Unmodified: Commit
</div>

---

<header>

# Les bases de Git
</header>

<center>

## Un Graphe
</center>


<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch toto
  checkout toto
  commit
  commit
  checkout main
  commit
  checkout toto
  commit
  checkout main
  merge toto
  commit
  branch tutu
  checkout tutu
  commit
  branch titi
  checkout titi
  commit
  checkout main
  commit
  checkout titi
  commit
  checkout tutu
  merge titi
  commit
  checkout main
  merge tutu
  commit
</div>

---

<header>

# Exercices
</header>

## Le QR Code de repo git pour les exercises

![image](qr_code_forma_git_2.png)

---

<header>

# Annexe
</header>

## Clés SSH

Une clé SSH permet d'authentifier une machine (ton PC en gros). Il faut un générer une paire :

- une clé publique : celle que vous partagez à tout le monde
- une clé privée : celle que vous gardez bien au chaud sur votre machine

---

<header>

# Annexe
</header>


![image](./Public_key_encryption.svg "title-2") ![image](./Private_key_signing.svg)

---

<header>

# Annexe
</header>

## Précision pour les exercises

Dans les commandes, une valeur entre chevrons doit être remplacée par une valeur de votre choix **chevrons compris !!!**

---

<header>

# Aller plus loin
</header>

## Les frontends

- [GitHub](https://github.com/)
- [GitLab](https://gitlab.com)
- [Gitea](https://gitea.io/)
- [Gogs](https://gogs.io/)
- [Codeberg](https://codeberg.org/)
- [SourceHut](https://sr.ht/)

---
<header>

# Aller plus loin
</header>

## Quelques outils

- [pre-commit](https://pre-commit.com/)
- [GitKraken](https://www.gitkraken.com/)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [CI/CD](https://resources.github.com/ci-cd/)
- [Signature](https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits)
